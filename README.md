# Weather Application Document

## Gitlab 
This project is published at the Gitlab, you can use it easily. :) \
Gitlab link: [Weather application](https://gitlab.com/Mohammad.Madani/weather.git).

Commands to run the project
```sh
git clone https://gitlab.com/Mohammad.Madani/weather.git
cd weather
yarn install 
```

Create the .env file at the root of the project and copy the information to it
``` js
PORT = 3000
DB_URL = mongodb://127.0.0.1:27017/weather
```

Create storage folder
```
project
│   
└─── storage
│       └─── weather
│       │       └───  daily-weather
│       │       └───  monthly-weather
```

Then run the project
``` sh
yarn start
```

## Server information
> baseUrl: `localhost:3000/api/v1`

| CONTROLLERS | URL | METHOD |
| ------ | ------ | ------ |
| getDailyWeather | /weather/daily-weather-forecast | GET
| getMonthlyWeather | /weather/monthly-weather-forecast | GET

## Responses structure
#### Success
```js
{
    "status": "Success",
    "data": "data"
}
```
#### Failed
```js
{
    "status": "Failed",
    "message": "error message",
    "errorCode": "error code"
}
```

## Controllers

#### getDailyWeather
> url: `/weather/daily-weather-forecast` \
> The JSON file will be saved in this path: `/storage/weather/daily-weather/filename.json`
```js
{
    "status": "Success",
    "data": "null"
}
```

#### getMonthlyWeather
> url: `/weather/monthly-weather-forecast` \
> The JSON file will be saved in this path: `/storage/weather/monthly-weather/filename.json`
```js
{
    "status": "Success",
    "data": "null"
}
```
