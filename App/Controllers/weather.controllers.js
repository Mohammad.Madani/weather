/* // weather.controllers.js // */

/* ============================== PACKAGES ==============================  */
const errorMessage = require('../Utils/errorMessage');
const successMessage = require('../Utils/successMessage');

const cheerio = require('cheerio');
const axios = require('axios');
const fs = require('fs');

/* ============================== CONTROLLERS ==============================  */
const getDailyWeather = async (req, res) => {
  try {
    const url = 'https://www.accuweather.com/en/gb/london/ec4a-2/daily-weather-forecast/328328'; // Daily weather url - Hard code
    let dailyWeatherData = [];
    let dateRange;

    // Uses axios to get html
    const { data } = await axios.get(url);

    // Uses Jquery (cheerio) to find classes
    let $ = cheerio.load(data);

    // Title => ex: September 20 - October 1
    $('.module-title').each((i, e) => {
      dateRange = $(e).text().trim();
    });

    // Data
    $('.daily-wrapper').each((i, e) => {
      const weekDay = $(e).find('.dow').text().trim();
      const date = $(e).find('.sub').text().trim();
      const phrase = $(e).find('.phrase').text().trim();
      const precip = $(e).find('.precip').text().trim();
      const temp = {
        highTemp: $(e).find('.high').text().trim(),
        lowTemp: $(e).find('.low').text().trim().replace('/', ''),
      };

      dailyWeatherData.push({ weekDay, date, temp, phrase, precip });
    });

    const dailyWeather = { dateRange, dailyWeatherData };

    // Deletes file and Writes file
    const filePath = `./storage/weather/daily-weather/${dateRange}.json`;

    fs.existsSync(filePath) && fs.unlinkSync(filePath);
    fs.appendFileSync(filePath, JSON.stringify(dailyWeather, null, '\t'), 'utf8');

    successMessage(res, null);
  } catch (error) {
    errorMessage(res, error);
  }
};

const getMonthlyWeather = async (req, res) => {
  try {
    const url = 'https://www.accuweather.com/en/gb/london/ec4a-2/september-weather/328328'; // monthly weather url - Hard code
    const monthlyWeathreData = { sunday: [], monday: [], tuesday: [], wednesday: [], thursday: [], friday: [], saturday: [] };
    let monthlyCalendarHeaders = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
    let month;
    let year;

    // Uses axios to get html
    const { data } = await axios.get(url);

    // Uses Jquery (cheerio) to find classes
    let $ = cheerio.load(data);

    // Month and Year
    const monthSelector = `body > div > div.two-column-page-content > div.page-column-1 > div.content-module > div.monthly-tools.non-ad > div.monthly-dropdowns > div:nth-child(1) > div.map-dropdown-toggle > h2`;
    const yearSelector = `body > div > div.two-column-page-content > div.page-column-1 > div.content-module > div.monthly-tools.non-ad > div.monthly-dropdowns > div:nth-child(2) > div.map-dropdown-toggle > h2`;

    $(monthSelector).each((i, e) => {
      month = $(e).text().trim();
    });

    $(yearSelector).each((i, e) => {
      year = $(e).text().trim();
    });

    // Calendar data
    let counter = 0;
    $('.monthly-daypanel').each((i, e) => {
      if (counter == 7) counter = 0;
      const date = $(e).find('.date').text().trim();
      const temp = { highTemp: $(e).find('.high').text().trim(), lowTemp: $(e).find('.low').text().trim() };
      const data = {
        date,
        temp,
      };

      // Concat data to obj
      monthlyWeathreData[monthlyCalendarHeaders[counter]] = monthlyWeathreData[monthlyCalendarHeaders[counter]].concat(data);
      counter++;
    });

    // Deletes file and Writes file
    const date = `${month} ${year}`;
    const monthlyData = { date, monthlyWeathreData };
    const filePath = `./storage/weather/monthly-weather/${date}.json`;

    fs.existsSync(filePath) && fs.unlinkSync(filePath);
    fs.appendFileSync(filePath, JSON.stringify(monthlyData, null, '\t'), 'utf8');

    successMessage(res, null);
  } catch (error) {
    errorMessage(res, error);
  }
};

/* ============================== EXPORTS ==============================  */
module.exports = {
  getDailyWeather,
  getMonthlyWeather,
};
