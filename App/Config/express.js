/* // express.js // */

/* ============================== PACKAGES ==============================  */
const cors = require('cors');
const express = require('express');
const morgan = require('morgan');
// const fileUpload = require('express-fileupload');
const router = require('../Routes/index');
const path = require('path');
const rateLimit = require('express-rate-limit');
const limiter = rateLimit({
  windowMs: 1 * 60 * 1000, // 1 minutes
  max: 30, // limit each IP to 100 requests per windowMs
});

/* ============================== EXPORTS ==============================  */
module.exports = function (app) {
  app.set('views', 'App/views');
  // app.set('view engine', 'ejs');
  app.use(express.json());
  app.use(cors());
  app.use(express.static(path.join(__dirname, '../../storage')));
  // app.use(fileUpload());
  app.disable('etags');
  app.use(morgan('dev'));
  app.use('/api', limiter);
  app.use('/api/v1', router);
  app.get('*', (req, res) => {
    res.status(404).sendFile(path.join(__dirname, '../../index.html'));
  });
};
