/* // index.js // */

require('dotenv').config();

/* ============================== EXPORTS ==============================  */
module.exports = {
  // Server
  PORT: process.env.PORT || 3000,
  DBURL: process.env.DB_URL,
};
