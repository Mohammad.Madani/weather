/* // weather.routes.js // */

/* ============================== PACKAGES ==============================  */
const weatherControllers = require('../Controllers/weather.controllers');
const prefix = '/weather';

/* ============================== EXPROTS ==============================  */
module.exports = function (router) {
  router.get(`${prefix}/daily-weather-forecast`, weatherControllers.getDailyWeather);
  router.get(`${prefix}/monthly-weather-forecast`, weatherControllers.getMonthlyWeather);
};
