/* // index.js // */

/* ============================== PACKAGES ==============================  */
const router = require('express')();

/* ===> ROUTERS */
require('./weather.routes')(router);

/* ============================== EXPORTS ==============================  */
module.exports = router;
