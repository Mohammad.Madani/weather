/* // errorMessage.js // */

/* ============================== PACKAGES ==============================  */
const log4js = require('log4js');
const { error } = require('consola');

/* ===> Log4js configure */
const logger = log4js.getLogger();
log4js.configure({
  appenders: { errorLog: { type: 'file', filename: 'errorLog.log' } },
  categories: { default: { appenders: ['errorLog'], level: 'error' } },
});

/* ============================== ERROR MESSAGE ==============================  */
const errorMessage = function (res, message, status = 500, errorCode = null) {
  if (res === null) {
    // console.log("ERROR ===> ", message);
    error({ message, badge: true });
    return logger.error(message);
  }

  error({ message, badge: true });
  res.status(status).send({ status: 'Failed', message: message, errorCode: errorCode });
  return logger.error(message);
};

/* ============================== EXPORTS ==============================  */
module.exports = errorMessage;
