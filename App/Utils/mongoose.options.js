/* // mongoose.options.js // */

const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: false,
};

/* ============================== EXPORTS ==============================  */
module.exports = options;
