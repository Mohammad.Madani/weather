/* // successMessage.js // */

/* ============================== SUCCESS MESSAGE ==============================  */
const successMessage = function (res, data) {
  res.status(200).send({ status: 'Success', data: data }).end();
};

/* ============================== EXPORTS ==============================  */
module.exports = successMessage;
