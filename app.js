/* // app.js // */

/* ============================== PACKAGES ==============================  */
const app = require('express')();
const errorMessage = require('./App/Utils/errorMessage');
const server = require('http').Server(app);
const { success, error } = require('consola');
const { PORT, DBURL } = require('./App/Config/index');
const { connect } = require('mongoose');
const options = require('./App/Utils/mongoose.options');
require('./App/Config/express')(app);

/* ===> SERVER */
async function startApp() {
  try {
    await connect(DBURL, options);
    server.listen(PORT, () => {
      success({ message: ' ', badge: true });
      success({ message: `Listening on port: ${PORT}` });
      success({ message: `database listening` });
    });
  } catch (error) {
    errorMessage(null, error);
  }
}

startApp();
